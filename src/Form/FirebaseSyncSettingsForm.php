<?php
/**
 * @file
 *  Contains Firebase Sync configuration settings.
 */
namespace Drupal\firebase_sync\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StreamWrapper\PrivateStream;
use Drupal\firebase_sync\FirebaseSyncHelper;


class FirebaseSyncSettingsForm extends ConfigFormBase {

  protected $supported_entities;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'firebase_sync_admin_settings';
  }

  public function __construct(\Drupal\Core\Config\ConfigFactoryInterface $config_factory) {
    $this->supported_entities = FirebaseSyncHelper::getSupportedBundles();
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'firebase_sync.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if (!PrivateStream::basePath()) {
      drupal_set_message($this->t('The private file storage path is not defined. You need to configure it from your settings.php file.'), 'error');
    }
    $config = $this->config('firebase_sync.settings');

    $form['supported_entities'] = array(
      '#type' => 'fieldset',
      '#title' => 'Choose content to push on Firebase',
      '#weight' => 5,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,

    );

    foreach ($this->supported_entities as $key => $supported_entity) {
      $actives = array_filter($config->get('supported_entities.' . $key), function ($var) {
        return (!empty($var));
      });
      $actives = array_keys($actives);
      $form['supported_entities'][$key] = array(
        '#type' => 'checkboxes',
        '#options' => $supported_entity,
        '#title' => $key,
        '#default_value' => $actives,
      );
    }

    $sync_mode = array(0 => t('Drupal Queue'), 1 => t('Direct'));
    $form['sync_mode'] = array(
      '#type' => 'radios',
      '#title' => t('Sync mode'),
      '#default_value' => 1,
      '#options' => $sync_mode,
      '#required' => TRUE,
      '#description' => '',
    );

    $form['firebase_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Firebase app base URL'),
      '#required' => TRUE,
      '#default_value' => $config->get('firebase_url'),
    );

    $form['service_account'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Google service account'),
      '#description' => $this->t('Paste here the Google Service account. Click <a href="@link" target="_blank">here</a> to generate one for your project.',
        ['@link' => 'https://firebase.google.com/docs/admin/setup#add_firebase_to_your_app']
      ),
      '#disabled' => (PrivateStream::basePath() ? FALSE : TRUE),
      '#required' => TRUE,
      '#default_value' => $config->get('service_account'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Store configuration as file in the private directory.
    /** @TODO implements validate methode to check if the private stream wrapper is available.
     *  If the private directory is just set or changed, you need to flush the cache first to make private
     *  stream wrapper available to the system.
     */
    $firebase_directory = 'private://firebase_sync/';
    file_prepare_directory($firebase_directory, FILE_CREATE_DIRECTORY);
    $file = file_save_data($form_state->getValue('service_account'), 'private://firebase_sync/adminsdk.json', FILE_EXISTS_RENAME);

    // Retrieve the configuration and store.
    $this->configFactory->getEditable('firebase_sync.settings')
      // Set the submitted configuration setting
      ->set('firebase_url', $form_state->getValue('firebase_url'))
      ->set('service_account', $form_state->getValue('service_account'))
      ->set('adminsdk_file', $file->getFileUri())
      ->set('sync_mode', $form_state->getValue('sync_mode'))
      ->save();

    foreach ($this->supported_entities as $key => $bundles) {
      foreach ($bundles as $bundle_key => $bundle) {
        $value = !empty($form_state->getValue([$key, $bundle_key])) ? TRUE : FALSE;
        $this->configFactory()->getEditable('firebase_sync.settings')
        ->set('supported_entities.' . $key . '.' . $bundle_key, $value)
        ->save();
      }

    }

    parent::submitForm($form, $form_state);

  }
}