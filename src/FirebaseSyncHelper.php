<?php
/**
 * Created by PhpStorm.
 * User: mouhammed
 * Date: 10/01/2018
 * Time: 17:52
 */

namespace Drupal\firebase_sync;


use Drupal\Core\Entity\EntityInterface;

class FirebaseSyncHelper {

  public static function getSupportedBundles() {
    $supported_entities = [];
    $entities = [
      'block_content_type' => 'block_content',
      'node_type' => 'node',
      'taxonomy_vocabulary' => 'taxonomy_term',
    ];

    foreach ($entities as $key => $entity) {
      $content_types = \Drupal::service('entity.manager')->getStorage($key)->loadMultiple();
      foreach ($content_types as $content_type) {
        $supported_entities[$entity][$content_type->id()] = $content_type->label();
      }
    }
    $supported_entities['user']['user'] = 'User';

    return $supported_entities;
  }

  public static function isBundleSyncable(EntityInterface $entity) {
    /**
     * @var \Drupal\Core\Config\ConfigFactoryInterface;
     */
    $config = \Drupal::service('config.factory')->getEditable('firebase_sync.settings');
    return $config->get('supported_entities.' . $entity->getEntityTypeId() . '.' . $entity->bundle());
  }
}