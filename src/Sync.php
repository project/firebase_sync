<?php
/**
 * Created by PhpStorm.
 * User: mouhammed
 * Date: 11/01/2018
 * Time: 15:17
 */

namespace Drupal\firebase_sync;


use Drupal\Core\Entity\EntityInterface;
use Kreait\Firebase\Configuration;
use Kreait\Firebase\Firebase;


class Sync {

  /**
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  private $entityManager;
  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  private $loggerChannelFactory;
  /**
   * The firebase library.
   *
   * @var \Kreait\Firebase\Firebase
   */
  private $firebase;
  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;
  /**
   * @var \Drupal\Core\File\FileSystem
   */
  private $fileSystem;
  /**
   * @var \Drupal\Core\Queue\QueueFactory
   */
  private $queueFactory;

  /**
   * Sync constructor.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entityManager
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerChannelFactory
   * @param \Kreait\Firebase\Firebase $firebase
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   */
  public function __construct(\Drupal\Core\Entity\EntityManagerInterface $entityManager, \Drupal\Core\Logger\LoggerChannelFactory $loggerChannelFactory, \Drupal\Core\Config\ConfigFactoryInterface $configFactory, \Drupal\Core\File\FileSystem $fileSystem, \Drupal\Core\Queue\QueueFactory $queueFactory) {
    $this->entityManager = $entityManager;
    $this->loggerChannelFactory = $loggerChannelFactory;
    $this->configFactory = $configFactory;
    $this->fileSystem = $fileSystem;
    $this->queueFactory = $queueFactory;

    $firebase_url = $this->configFactory->getEditable('firebase_sync.settings')->get('firebase_url');
    $config_file = $this->configFactory->getEditable('firebase_sync.settings')->get('adminsdk_file');
    $config_file = $this->fileSystem->realpath($config_file);
    $config = new Configuration();
    $config->setAuthConfigFile($config_file);
    $this->firebase = new Firebase($firebase_url, $config);
  }


  public function set(EntityInterface $entity) {
    if($this->configFactory->getEditable('firebase_sync.settings')->get('sync_mode') == 1) {
      $this->sync($entity, 'set');
    } else { // Add to queue to sync content by cronjob
      $this->addToQueue($entity, 'set');
    }
  }
  public function delete(EntityInterface $entity) {
    if($this->configFactory->getEditable('firebase_sync.settings')->get('sync_mode') == 1) {
      $this->sync($entity, 'delete');
    } else { // Add to queue to sync content by cronjob
      $this->addToQueue($entity, 'delete');
    }
  }

  public function addToQueue(EntityInterface $entity, $op) {
    $queue = $this->queueFactory->get('firebase_sync_entities');
    $data = ['entity' => $entity, 'op' => $op];
    $queue->createItem($data);
  }

  public function sync(EntityInterface $entity, $op) {
    if($op == 'set') {

      $this->firebase->set($entity->toArray(), $entity->getEntityTypeId() . '_' . $entity->bundle() . '/' . $entity->id());
    }
    if($op == 'delete') {
      $this->firebase->delete($entity->getEntityTypeId() . '_' . $entity->bundle() . '/' . $entity->id());
    }

  }


}